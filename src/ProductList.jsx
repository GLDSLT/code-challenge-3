import React from 'react';
import { Container, Row, Col, Card, CardImg, CardText, CardBody,
  CardTitle, Button } from 'reactstrap';
import AppHome from './AppHome';
import axios from 'axios';
 
class ProductList extends React.Component{
    constructor(){
        super();
        this.state = {
            products:[]
        }
    }
componentDidMount(){
    axios.get(`http://reduxblog.herokuapp.com/api/posts?key=eva`)
        .then(response => {
            this.setState({
                products: response.data
            })
        })
}

handleDelete = productId => {
    const products = [...this.state.products];
    axios.delete(`http://reduxblog.herokuapp.com/api/posts/${productId}`)
    .then(res => {
        console.log(res);
        alert('removed');
        this.setState({products:products});
        window.location.reload();
    } )
    .catch(err => {
        console.log(err)
    })
}

    render() {
        console.log(this.state.products)
        return (
            <div>
                <Container>
                    <AppHome />
                    <Row className='d-flex'>
                        {
                            this.state.products.map(( product) =>
                            <Col key={product.id} md='4'>
                                <Card>
                                    <CardImg top width="100%" src={product.categories} alt="Card image cap" />
                                    <CardBody>
                                    <CardTitle>{product.title}</CardTitle>
                                    <CardText>{product.content}</CardText>
                                    <Button color='danger' onClick={(e) => this.handleDelete(product.id)}>
                                    Delete
                                    </Button>
                                    </CardBody>
                                </Card>
                            </Col>
                        )
                        }

                    </Row>
                </Container>
            </div>
       )
   }
}

 
export default ProductList;