import React from 'react';
import AppHome from './AppHome';

import {Container, Row, Button, Form, FormGroup} from 'reactstrap';
import axios from 'axios';

class AddProduct extends React.Component{

    constructor(props){
        super(props);
        this.state = {
                title:'',
                categories: '',
                content: '',
        }
    }

    handleTitleChange = event => {this.setState({ title: event.target.value })};
    handleCategoriesChange = event => {this.setState({ categories: event.target.value })};
    handleContentChange = event => {this.setState({ content : event.target.value })};

    handleSubmit = event => {
        event.preventDefault();
        axios.post(`http://reduxblog.herokuapp.com/api/posts?key=eva`,
            {title: this.state.title, categories: this.state.categories, content:this.state.content },)
        .then(res => {
            console.log(res);
            console.log(res.data);
            alert('article was added')
            
            window.location.reload('/');
        })
    }

    render(){
        console.log('test',this.state.title)
        return(
            <div>
                <AppHome />
                <Container>
                    <Row>
                        <div>
                            <div><h1>Add product data</h1></div>
                            <Form onSubmit={this.handleSubmit}>
                                <FormGroup>
                                    <label>
                                        Title: <input type="text"
                                                      name="this.state.title"
                                                      onChange={this.handleTitleChange} />
                                    </label>
                                </FormGroup>
                                <FormGroup>
                                    <label>
                                        Image: <input type="text"
                                                      name="this.state.categories"
                                                      onChange={this.handleCategoriesChange} />
                                    </label>
                                </FormGroup>
                                <FormGroup>
                                    <label>
                                        Content: <input type="text"
                                                        name="this.state.content"
                                                        onChange={this.handleContentChange} />
                                    </label>
                                </FormGroup>
                                <Button color="primary" type="submit"> Add Data </Button>
                            </Form>
                        </div>
                    </Row>
                </Container>
                </div>
        )
    }
}

export default AddProduct;