import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import ProductList from './ProductList';
import {BrowserRouter, Route} from 'react-router-dom'
import AddProduct from './AddProduct';


class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
        <div>
          <Route exact path="/" component={AddProduct} />
          <Route exact path="/product" component={ProductList} />
        </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
